#!/usr/bin/env python3

import shutil
import fileinput
import time

def main() -> None:
    start_time = time.time()

    print()

    last_line_from_build = False
    total_warnings = 0
    total_errors = 0

    for line in fileinput.input():
        width = shutil.get_terminal_size((80, 24)).columns

        if line.startswith("["):
            # Give some spacing between build info and the status
            if last_line_from_build:
                print("\n")
                last_line_from_build = False

            partitioned = line[1:].partition("/")
            completed = int(partitioned[0])
            total = int(partitioned[2].partition("]")[0])

            if line.partition("] ")[2].startswith("Building"):
                color_str = "\033[32;1m"  # Bright green
            else:
                color_str = "\033[34;1m"  # Bright blue

            progress_str = "["
            progress_str += " " * (len(str(total)) - len(str(completed)))
            progress_str += str(completed) + "/" + str(total)
            progress_str += "]"

            print("\r\033[1A\033[0K\033[0m" + progress_str + " " + color_str + line.partition("] ")[2] + "\033[0m")
            print("\r\033[1A" + get_progress_bar(completed / total, width, color_str), end="", flush=True)
        else:
            last_line_from_build = True

            pointer_chars = set("^~ ")

            if set(line.strip()).issubset(pointer_chars):
                print("\033[32;1m" + line + "\033[0m", end="", flush=True)
            else:
                split = line.split(" ")
                if len(split) == 3 and split[0].isdigit() and (split[1] == "warning" or split[1] == "warnings") and split[2] == "generated.\n":
                    # It's an x warnings generated message
                    print("\033[35;1m" + line + "\033[0m", end="", flush=True)

                    total_warnings += int(split[0])

                elif len(split) == 3 and split[0].isdigit() and (split[1] == "error" or split[1] == "errors") and split[2] == "generated.\n":
                    # It's an x warnings generated message
                    print("\033[31;1m" + line + "\033[0m", end="", flush=True)

                    total_errors += int(split[0])

                elif len(split) == 6 and split[0].isdigit() and (split[1] == "warning" or split[1] == "warnings") and split[2] == "and" and split[3].isdigit() and (split[4] == "error" or split[4] == "errors") and split[5] == "generated.\n":
                    # It's an x warnings and x errors generated message
                    print("\033[31;1m" + line + "\033[0m", end="", flush=True)

                    total_warnings += int(split[0])
                    total_errors += int(split[3])

                else:
                    split_colon = line.split(":")
                    if len(split_colon) >= 5 and split_colon[1].isdigit() and split_colon[2].isdigit():
                        # It's a warning/error message
                        if split_colon[3] == " warning":
                            split_colon[3] = "\033[35;1m warning\033[0;1m"
                        elif split_colon[3] == " error":
                            split_colon[3] = "\033[31;1m error\033[0;1m"
                        elif split_colon[3] == " note":
                            split_colon[3] = "\033[30;1m note\033[0;1m"

                        print("\033[;1m" + ":".join(split_colon) + "\033[0m", end="", flush=True)

                    else:
                        print(line, end="", flush=True)

    if total_errors > 0:
        finished_color_str = "\033[31;1m"  # Bright red
    elif total_warnings > 0:
        finished_color_str = "\033[35;1m"  # Bright purple
    else:
        finished_color_str = "\033[32;1m"  # Bright green

    execution_time = time.time() - start_time

    print("\n" + finished_color_str + "Build finished in {time:.3f}s with {warnings} warnings and {errors} errors\033[0m".format(
        time=execution_time,
        warnings=total_warnings,
        errors=total_errors
        ))

def get_progress_bar(percent: float, width: int, ansi_color: str) -> str:
    bar_filled_length = int(percent * (width - 2))
    bar_empty_length = width - 2 - bar_filled_length

    out_str = ansi_color + "["
    out_str += "=" * bar_filled_length
    out_str += "\033[30;1m" + "=" * bar_empty_length
    out_str += ansi_color + "]\033[0m"

    return out_str

if __name__ == "__main__":
    main()

