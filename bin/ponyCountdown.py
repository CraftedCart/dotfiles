#!/usr/bin/env python2
#Requires dateutil and pytz
#For example - On Arch Linux, do pacman -S python2-dateutil python2-pytz

import urllib2
import json
import datetime
import dateutil.parser
import pytz
import time

contents = urllib2.urlopen("http://api.ponycountdown.com/").read()
data = json.loads(contents)

nextTime = None

def getNextTime():
    global nextTime

    for item in data:
        timestr = item["time"]
        # showTime = datetime.datetime.strptime(timestr, "%Y-%m-%dT%H:%M:%S.%fZ")
        showTime = dateutil.parser.parse(timestr)
        nowTime = datetime.datetime.now(pytz.utc)
        if showTime > nowTime:
            nextTime = showTime
            break

getNextTime()

while True:
    nowTime = datetime.datetime.now(pytz.utc)

    if (nextTime < nowTime):
        getNextTime()

    deltaTime = nextTime - nowTime
    deltaTime = deltaTime - datetime.timedelta(microseconds=deltaTime.microseconds)
    print(deltaTime)

    time.sleep(1)

