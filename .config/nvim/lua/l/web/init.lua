--- Web layer
-- @module l.web

local plug = require("c.plug")
local autocmd = require("c.autocmd")
local file = require("c.file")
local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")

local layer = {}

local function on_filetype_web()
  vim.api.nvim_buf_set_option(0, "shiftwidth", 2)
  vim.api.nvim_buf_set_option(0, "tabstop", 2)
  vim.api.nvim_buf_set_option(0, "softtabstop", 4)

  -- keybind.bind_command(edit_mode.INSERT, "<C-tab>", "emmet#expandAbbrIntelligent('<C-tab>')", { expr = true })
end

local function on_filetype_jinja()
  vim.b.AutoPairs = vim.fn.AutoPairsDefine{
    ["{%"] = "%}",
  }
end

-- local function activate_emmet()
  -- vim.cmd("EmmetInstall")
-- end

--- Returns plugins required for this layer
function layer.register_plugins()
  -- plug.add_plugin("mattn/emmet-vim") -- Nifty completion stuffs
  plug.add_plugin("Glench/Vim-Jinja2-Syntax") -- Templating syntax
  -- plug.add_plugin("lilydjwg/colorizer") -- Highlight color codes (This makes switching to large buffers really slow ><)
  plug.add_plugin("alvan/vim-closetag") -- Auto-close HTML tags
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local lspconfig = require("lspconfig")

  lsp.register_server(lspconfig.html)

  autocmd.bind_filetype("html", on_filetype_web)
  autocmd.bind_filetype("css", on_filetype_web)
  autocmd.bind_filetype("scss", on_filetype_web)
  autocmd.bind_filetype("javascript", on_filetype_web)
  autocmd.bind_filetype("jinja", on_filetype_web)
  autocmd.bind_filetype("html.handlebars", on_filetype_web)
  autocmd.bind_filetype("vue", on_filetype_web)
  autocmd.bind_filetype("eruby", on_filetype_web)

  autocmd.bind_filetype("jinja", on_filetype_jinja)

  -- Emmet config
  -- vim.g.user_emmet_install_global = 0
  -- autocmd.bind_filetype("html", activate_emmet)
  -- autocmd.bind_filetype("css", activate_emmet)
  -- autocmd.bind_filetype("scss", activate_emmet)
  -- autocmd.bind_filetype("jinja", activate_emmet)
  -- autocmd.bind_filetype("html.handlebars", activate_emmet)
  -- autocmd.bind_filetype("vue", activate_emmet)

  -- closetag config
  vim.g.closetag_filenames = "*.html,*.xhtml,*.phtml,*.html.erb"
  vim.g.closetag_xhtml_filenames = "*.xhtml,*.jsx"
  vim.g.closetag_filetypes = "html,xhtml,phtml,eruby"
  vim.g.closetag_xhtml_filetypes = "xhtml,jsx"
  vim.g.closetag_emptyTags_caseSensitive = 1

  vim.g.closetag_regions = {
    ["typescript.tsx"] = "jsxRegion,tsxRegion",
    ["javascript.jsx"] = "jsxRegion",
  }

  vim.g.closetag_shortcut = ">"

  -- Terra templating is similar to Jinja
  file.set_filetype_for("*.tera", "jinja")

  -- Ignore the black hole
  file.add_to_wildignore("node_modules")
end

return layer
