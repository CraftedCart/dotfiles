--- Lisps layer
-- @module l.lisps

local autocmd = require("c.autocmd")
-- local keybind = require("c.keybind")
local plug = require("c.plug")

local layer = {}

local function on_filetype_any_lisp()
  -- Don't try and auto-pair parens or single quotes
  vim.b.AutoPairs = vim.fn.AutoPairsDefine{
    ["("] = "",
    ["'"] = "",
  }

  -- 2 space indent
  vim.api.nvim_buf_set_option(0, "shiftwidth", 2)
  vim.api.nvim_buf_set_option(0, "tabstop", 2)
  vim.api.nvim_buf_set_option(0, "softtabstop", 2)
end

--- Registers plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("Olical/conjure")
  plug.add_plugin("m00qek/completion-conjure") -- Hook up conjure to completion-nvim
  -- plug.add_plugin("lisps-vim/vim-jack-in")
  plug.add_plugin("bhurlow/vim-parinfer")
end

--- Configures vim and plugins for this layer
function layer.init_config()
  autocmd.bind_filetype("clojure", on_filetype_any_lisp)
  autocmd.bind_filetype("racket", on_filetype_any_lisp)
  autocmd.bind_filetype("scheme", on_filetype_any_lisp)

  -- Indenting in lisp is all over the place - indent guides don't help here
  -- table.insert(vim.g.indent_guides_exclude_filetypes, "lisps") doesn't work here
  local excludes = vim.g.indent_guides_exclude_filetypes
  table.insert(excludes, "lisps")
  vim.g.indent_guides_exclude_filetypes = excludes

  local completion_chain = vim.g.completion_chain_complete_list or {}
  completion_chain["clojure"] = {
    -- {complete_items = { "conjure", "lsp", "snippet" }},
    {complete_items = { "conjure", "snippet" }},
  }
  completion_chain["racket"] = {
    {complete_items = { "conjure", "snippet" }},
  }
  vim.g.completion_chain_complete_list = completion_chain

  -- Add helper info to Conjure keybinds
  -- TODO: Figure out something that works for localleader
  -- keybind.add_info("<localleader>c", "Conjure client")
  -- keybind.add_info("<localleader>cs", "Start REPL")
  -- keybind.add_info("<localleader>cS", "Stop REPL")
end

return layer
