--- C# layer
-- @module l.csharp

local autocmd = require("c.autocmd")
local file = require("c.file")

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local lspconfig = require("lspconfig")

  lsp.register_server(
    lspconfig.omnisharp,
    {
      cmd = {file.make_full("~/.cache/nvim/lspconfig/omnisharp/run")},
    }
    )
end

return layer
