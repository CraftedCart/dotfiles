--- Haskell layer
-- @module l.haskell

local plug = require("c.plug")

local layer = {}

--- Registers plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("neovimhaskell/haskell-vim")
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local build = require("l.build")
  local lspconfig = require("lspconfig")

  local lsp_installer_servers = require("nvim-lsp-installer.servers")

  local server_available, requested_server = lsp_installer_servers.get_server("hls")
  if server_available then
    requested_server:on_ready(function()
      lsp.register_server(lspconfig.hls, requested_server:get_default_options())
    end)
    -- if not requested_server:is_installed() then
      -- -- Queue the server to be installed
      -- requested_server:install()
    -- end
  end

  build.make_builder()
    :with_filetype("haskell")
    :with_prerequisite_file("stack.yaml")
    :with_build_command("stack build")
    :with_test_command("stack test")
    :add()

  -- Configure neovimhaskell/haskell-vim
  vim.g.haskell_indent_if = 3
  vim.g.haskell_indent_case = 2
  vim.g.haskell_indent_let = 4
  vim.g.haskell_indent_where = 6
  vim.g.haskell_indent_before_where = 2
  vim.g.haskell_indent_after_bare_where = 2
  vim.g.haskell_indent_do = 3
  vim.g.haskell_indent_in = 1
  vim.g.haskell_indent_guard = 2
end

return layer
