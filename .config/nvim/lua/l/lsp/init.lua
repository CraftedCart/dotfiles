--- Language server protocol support, courtesy of Neovim
-- @module l.lsp

local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")
local autocmd = require("c.autocmd")
local plug = require("c.plug")
local class = require("c.class")
local Signal = require("c.signal").Signal

local layer = {}

local ClientProgress = class.strict {
  title = "...",
  message = class.NULL,
  percentage = class.NULL,
}

layer.client_progress = {}
layer.signal_progress_update = Signal()

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("neovim/nvim-lspconfig")
  plug.add_plugin("williamboman/nvim-lsp-installer")

  -- plug.add_plugin("nvim-lua/completion-nvim")

  -- Using this fork while waiting on this fork to be merged in
  -- https://github.com/nvim-lua/completion-nvim/pull/400
  -- plug.add_plugin("rafaelsq/completion-nvim", {branch = "changeHandlerSignature"})

  plug.add_plugin("ms-jpq/coq_nvim", {branch = "coq"})
end

local function user_stop_all_clients()
  local clients = vim.lsp.get_active_clients()

  if #clients > 0 then
    vim.lsp.stop_client(clients)
    for _, v in pairs(clients) do
      print("Stopped LSP client " .. v.name)
    end
  else
    print("No LSP clients are running")
  end
end

local function user_attach_client()
  local filetype = vim.bo[0].filetype

  local server = layer.filetype_servers[filetype]
  if server ~= nil then
    print("Attaching LSP client " .. server.name .. " to buffer")
    server.manager.try_add()
  else
    print("No LSP client registered for filetype " .. filetype)
  end
end

local function on_progress(err, result, ctx, config)
  if err ~= nil then return end

  if result.value.kind == "end" then
    layer.client_progress[ctx.client_id] = nil
  else
    local prog = layer.client_progress[ctx.client_id]
    if prog == nil then
      prog = ClientProgress()
      layer.client_progress[ctx.client_id] = prog
    end

    prog.title = result.value.title or prog.title
    prog.message = result.value.message or prog.message

    -- So the LSP spec says percentage should be a value between 0 - 100
    -- but some clients (like ccls) give a value between 0 - 1
    -- so here's a kludgey workaround
    if result.value.percentage ~= nil then
      local percent = result.value.percentage
      if percent <= 1 then
        prog.percentage = percent
      else
        prog.percentage = percent / 100
      end
    end
  end


  layer.signal_progress_update:emit()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- We want to recieve progress messages
  vim.lsp.handlers['$/progress'] = on_progress

  -- vim.api.nvim_set_var("completion_enable_in_comemnt", 1)
  -- vim.api.nvim_set_var("completion_confirm_key", "") -- I just use tab, plus also this conflicts with auto-pairs
  -- vim.api.nvim_set_var("completion_trigger_on_delete", 1)

  -- if plug.has_plugin("ultisnips") then
    -- vim.g.completion_enable_snippet = "UltiSnips"
  -- end

  vim.g.coq_settings = {
    auto_start = true,
    display = {
      ghost_text = {
        enabled = true,
        context = {" < ", " > "},
      },
      icons = {
        -- mode = "none", -- Show text only since I don't fancy installing a nerdfont
        mode = "long",
        mappings = {
          Boolean = "✓",
          Character = "c",
          Class = "●",
          Color = "",
          Constant = "⭤",
          Constructor = "⁜",
          Enum = "☰",
          EnumMember = "☷",
          Enumerator = "☷",
          Event = "",
          Field = "♦",
          File = "=",
          Folder = "≔",
          Function = "∫",
          Interface = "İ",
          Keyword = "",
          Method = "∫",
          Module = "⟃",
          Number = "⁐",
          Operator = "↦",
          Parameter = "",
          Property = "⊡",
          Reference = "↳",
          Snippet = "≡",
          String = "⊤",
          Struct = "◎",
          Text = "⊤",
          Typedef = "↩",
          TypeParameter = "",
          Unit = "○",
          Value = "⋆",
          Variable = "▷",
        },
      },
      pum = {
        -- source_context = {" [", "]"},
        kind_context = {" ", ""},
        source_context = {" ", ""},
        fast_close = true,
      },
      preview = {
        border = {
          {"", "NormalFloat"},
          {"", "NormalFloat"},
          {"", "NormalFloat"},
          {" ", "NormalFloat"},
          {"", "NormalFloat"},
          {"", "NormalFloat"},
          {"", "NormalFloat"},
          {" ", "NormalFloat"},
        },
      },
    },
  }

  -- Bind leader keys
  keybind.set_group_name("<leader>l", "LSP")
  keybind.bind_function(edit_mode.NORMAL, "<leader>ls", user_stop_all_clients, nil, "Stop all LSP clients")
  keybind.bind_command(edit_mode.NORMAL, "<leader>lS", ":LspRestart<cr>", nil, "Restart all LSP clients")
  keybind.bind_function(edit_mode.NORMAL, "<leader>la", user_attach_client, nil, "Attach LSP client to buffer")

  -- Tabbing
  -- keybind.bind_command(edit_mode.INSERT, "<tab>", "<Plug>(completion_smart_tab)", { silent = true })
  -- keybind.bind_command(edit_mode.INSERT, "<S-tab>", "<Plug>(completion_smart_s_tab)", { silent = true })
  -- autocmd.bind_complete_done(function()
    -- if vim.fn.pumvisible() == 0 then
      -- vim.cmd("pclose")
    -- end
  -- end)

  vim.o.completeopt = "menuone,noinsert,noselect"

  -- Jumping to places and show docs keybinds
  autocmd.bind_filetype("*", function()
    local server = layer.filetype_servers[vim.bo.ft]
    if server ~= nil then
      keybind.buf_bind_command(edit_mode.NORMAL, "gd", ":lua vim.lsp.buf.declaration()<CR>", { noremap = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "gD", ":lua vim.lsp.buf.implementation()<CR>", { noremap = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "<C-]>", ":lua vim.lsp.buf.definition()<CR>", { noremap = true })
      keybind.buf_bind_command(edit_mode.NORMAL, "K", ":lua vim.lsp.buf.hover()<CR>", { noremap = true })
      -- keybind.bind_command(edit_mode.NORMAL, "<C-k>", ":lua vim.lsp.buf.signature_help()<CR>", { noremap = true })

      keybind.buf_bind_command(edit_mode.NORMAL, "<M-CR>", ":lua vim.lsp.buf.code_action()<CR>", { noremap = true }, "Code action")

      vim.bo.omnifunc = "v:lua.vim.lsp.omnifunc"
    end
  end)

  keybind.bind_command(edit_mode.NORMAL, "<leader>lr", ":lua vim.lsp.buf.references()<CR>", { noremap = true }, "Find references")
  keybind.bind_command(edit_mode.NORMAL, "<leader>lR", ":lua vim.lsp.buf.rename()<CR>", { noremap = true }, "Rename")
  keybind.bind_command(edit_mode.NORMAL, "<leader>ld", ":lua vim.lsp.buf.document_symbol()<CR>", { noremap = true }, "Document symbol list")

  keybind.set_group_name("<leader>j", "Jump")
  keybind.bind_command(edit_mode.NORMAL, "<leader>jd", ":lua vim.lsp.buf.declaration()<CR>", { noremap = true }, "Jump to declaration")
  keybind.bind_command(edit_mode.NORMAL, "<leader>ji", ":lua vim.lsp.buf.implementation()<CR>", { noremap = true }, "Jump to implementation")
  keybind.bind_command(edit_mode.NORMAL, "<leader>jf", ":lua vim.lsp.buf.definition()<CR>", { noremap = true }, "Jump to definition")

  -- Show docs when the cursor is held over something
  -- autocmd.bind_cursor_hold(function()
    -- vim.cmd("lua vim.lsp.buf.hover()")
  -- end)

  -- Setup autocompletion in all buffers
  -- autocmd.bind_buf_enter(function()
    -- local completion = require("completion") -- From completion-nvim
    -- completion.on_attach()
  -- end)

  -- Setup vim.diagnostic config
  vim.diagnostic.config({
    underline = true,
    virtual_text = true,
    signs = true,
    update_in_insert = true,
    severity_sort = true,
  })

  -- Add diagnostics to the quickfix list
  autocmd.bind_diagnostics_changed(function()
    vim.diagnostic.setqflist{open = false}
  end)
end

--- Maps filetypes to their server definitions
--
-- <br>
-- Eg: `["rust"] = lspconfig.rls`
--
-- <br>
-- See `lspconfig` for what a server definition looks like
layer.filetype_servers = {}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.window = capabilities.window or {}
capabilities.window.workDoneProgress = true

--- Register an LSP server
--
-- @param server An LSP server definition (in the format expected by `lspconfig`)
-- @param config The config for the server (in the format expected by `lspconfig`)
function layer.register_server(server, config)
  config = config or {}
  config = vim.tbl_extend("keep", config, server.document_config.default_config)

  -- We want to recieve progress messages
  config.capabilities = vim.tbl_extend('keep', config.capabilities or {}, capabilities)

  server.setup(config)

  for _, v in pairs(config.filetypes) do
    layer.filetype_servers[v] = server
  end
end

return layer
