--- Activity Watch layer
-- https://github.com/ActivityWatch/activitywatch
-- @module l.activity_watch

local plug = require("c.plug")

local layer = {}

--- Registers plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("ActivityWatch/aw-watcher-vim")
end

--- Configures vim and plugins for this layer
function layer.init_config()
end

return layer
