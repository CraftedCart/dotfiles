--- Ruby on Rails layer
-- @module l.ruby_on_rails

local plug = require("c.plug")

local layer = {}

--- Registers plugins required for this layer
function layer.register_plugins()
  -- plug.add_plugin("tpope/vim-rails")
end

--- Configures vim and plugins for this layer
function layer.init_config()
end

return layer
