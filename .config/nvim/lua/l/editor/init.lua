--- User-specific editor tweaks
-- @module l.editor

local plug = require("c.plug")
local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")
local autocmd = require("c.autocmd")

local layer_man = require("l.editor.layer_man")

local layer = {}

-- The startup window doesn't seem to pick up on vim.o changes >.<
local function set_default_win_opt(name, value)
  vim.o[name] = value
  autocmd.bind_vim_enter(function()
    vim.wo[name] = value
  end)
end

-- The startup buffer doesn't seem to pick up on vim.o changes >.<
local function set_default_buf_opt(name, value)
  vim.o[name] = value
  autocmd.bind_vim_enter(function()
    vim.bo[name] = value
  end)
end

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("sheerun/vim-polyglot") -- A bunch of languages
  plug.add_plugin("tpope/vim-surround") -- Awesome for dealing with surrounding things, like parens
  plug.add_plugin("jiangmiao/auto-pairs") -- Auto insert matching parens/quotes/stuff
  plug.add_plugin("tpope/vim-endwise") -- Auto insert matching end keywords
  plug.add_plugin("preservim/nerdcommenter") -- Commenting
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- Space for leader, backslash for local leader
  vim.g.mapleader = " "
  vim.g.maplocalleader = "\\"

  -- Don't bind default keybinds
  vim.g.AutoPairsShortcutToggle = ""

  -- Save undo history
  set_default_buf_opt("undofile", true)

  -- Allow a .vimrc file in a project directory with safe commands
  vim.o.exrc = true
  vim.o.secure = true

  -- Use `fd` to exit insert/visual/select/terminal mode
  keybind.bind_command(edit_mode.INSERT, "fd", "<esc>", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "fd", "<esc>", { noremap = true })
  keybind.bind_command(edit_mode.TERMINAL, "fd", "<C-\\><C-n>", { noremap = true })

  -- More convenient tabs
  keybind.bind_command(edit_mode.NORMAL, "<M-=>", ":tabnew<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-->", ":tabclose<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-[>", ":tabprevious<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-]>", ":tabnext<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-{>", ":tabmove -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-}>", ":tabmove +1<CR>", { noremap = true })

  -- H/L to jump to the start/end of a line
  keybind.bind_command(edit_mode.NORMAL, "H", "^", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "H", "^", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "L", "$", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "L", "$", { noremap = true })

  -- Holding down Ctrl with j/k makes cursor movement 4x faster
  keybind.bind_command(edit_mode.NORMAL, "<C-j>", "4j", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<C-k>", "4k", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-j>", "4j", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<C-k>", "4k", { noremap = true })

  -- Holding down Ctrl with h/l finds the prev/next paren
  keybind.bind_command(edit_mode.NORMAL, "<C-h>", "?[(|)|\\[|\\]|{|}|<|>]<CR>:nohlsearch<CR>", { noremap = true, silent = true })
  keybind.bind_command(edit_mode.NORMAL, "<C-l>", "/[(|)|\\[|\\]|{|}|<|>]<CR>:nohlsearch<CR>", { noremap = true, silent = true })

  -- M-h/j/k/l to resize windows
  keybind.bind_command(edit_mode.NORMAL, "<M-h>", ":vertical resize -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-j>", ":resize -1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-k>", ":resize +1<CR>", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-l>", ":vertical resize +1<CR>", { noremap = true })

  -- M-p to paste the last yanked (and only yanked, not deleted) text
  keybind.bind_command(edit_mode.NORMAL, "<M-p>", "\"0p", { noremap = true })
  keybind.bind_command(edit_mode.NORMAL, "<M-P>", "\"0P", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<M-p>", "\"0p", { noremap = true })
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<M-P>", "\"0P", { noremap = true })

  -- Replace the word under the cursor
  keybind.bind_command(edit_mode.NORMAL, "<leader>R", ":%s/\\C\\<<C-r><C-w>\\>//g<Left><Left>", { noremap = true }, "Replace word under cursor")

  -- `,,` to insert an arrow `->`
  keybind.bind_command(edit_mode.INSERT, ",,", "->", { noremap = true })
  -- `,.` to insert a different arrow `=>`
  keybind.bind_command(edit_mode.INSERT, ",.", "=>", { noremap = true })

  -- Edit config, reload config, and update plugins
  keybind.set_group_name("<leader>fe", "Editor")
  keybind.bind_command(edit_mode.NORMAL, "<leader>fed", ":edit $HOME/.config/nvim<CR>", { noremap = true }, "Edit config")
  keybind.bind_command(edit_mode.NORMAL, "<leader>feR", ":source $MYVIMRC<CR>", { noremap = true }, "Reload config")
  keybind.bind_command(edit_mode.NORMAL, "<leader>feU", ":source $MYVIMRC|PlugUpgrade|PlugClean|PlugUpdate|source $MYVIMRC<CR>",
    {noremap = true}, "Install and update plugins")

  -- Buffer
  keybind.set_group_name("<leader>b", "Buffers")
  keybind.bind_command(edit_mode.NORMAL, "<leader>bn", ":enew<CR>", { noremap = true }, "New buffer")

  -- Default indentation rules
  set_default_buf_opt("tabstop", 4)
  set_default_buf_opt("softtabstop", 4)
  set_default_buf_opt("shiftwidth", 4)
  set_default_buf_opt("expandtab", true) -- Use spaces instead of tabs
  set_default_buf_opt("autoindent", true)
  set_default_buf_opt("smartindent", true)

  -- NERDCommenter config
  vim.g.NERDCreateDefaultMappings = 0 -- Don't add default keybinds
  vim.g.NERDSpaceDelims = 1 -- Add a space after the comment delimiter
  vim.g.NERDDefaultAlign = "left"
  keybind.set_group_name("<leader>c", "Comments")
  keybind.bind_command(edit_mode.NORMAL, "<leader>cl", ":call nerdcommenter#Comment('n', 'toggle')<CR>", { noremap = true, silent = true }, "Toggle line comment")
  keybind.bind_command(edit_mode.VISUAL_SELECT, "<leader>cl", ":call nerdcommenter#Comment('x', 'toggle')<CR>", { noremap = true, silent = true }, "Toggle line comment")

  -- A shortcut command for :lua print(vim.inspect(...)) (:Li for Lua Inspect)
  vim.cmd("command! -nargs=+ Li :lua print(vim.inspect(<args>))")

  -- A shortcut command to unload a Lua module (:Lu for Lua Unload)
  vim.cmd("command! -nargs=+ Lu :lua if package.loaded['<args>'] ~= nil then package.loaded['<args>'] = nil print('Unloaded Lua module: <args>') else print('Lua module <args> is not loaded') end")

  -- Use triple braces for folding
  set_default_win_opt("foldmethod", "marker")

  -- Show line at column 120, and wrap at 120
  set_default_win_opt("colorcolumn", "120")
  set_default_buf_opt("textwidth", 120)

  -- Update the window title
  vim.o.title = true

  -- Debug drawing
  keybind.bind_function(edit_mode.NORMAL, "<F3>", function()
    if vim.o.redrawdebug == "compositor" then
      vim.o.redrawdebug = ""
      vim.o.writedelay = 0
      print("Debug drawing disabled")
    else
      vim.o.redrawdebug = "compositor"
      vim.o.writedelay = 10
      print("Debug drawing enabled")
    end
  end, { noremap = true, silent = true }, "Toggle debug drawing")

  -- M-e to eval Lua on the current line
  keybind.bind_function(edit_mode.NORMAL, "<M-e>", function()
    local cursor = vim.api.nvim_win_get_cursor(0)
    local line = vim.api.nvim_buf_get_lines(0, cursor[1] - 1, cursor[1], false)[1]

    -- TODO: pcall this
    local value = loadstring("return " .. line)()
    vim.api.nvim_put({tostring(value)}, "l", true, false)

    print("Evaluated Lua")
  end, { noremap = true })

  -- M-e to eval Lua on the visual selection
  keybind.bind_function(edit_mode.VISUAL_SELECT, "<M-e>", function()
    local start_row, _, end_row, _ = edit_mode.visual_range()
    local lines = vim.api.nvim_buf_get_lines(0, start_row, end_row + 1, false)

    -- TODO: pcall this
    local value = loadstring(table.concat(lines, "\n"))()
    vim.api.nvim_put({tostring(value)}, "l", true, false)

    -- print("Evaluated Lua")
  end, { noremap = true })

  layer_man.init_config()
end

return layer
