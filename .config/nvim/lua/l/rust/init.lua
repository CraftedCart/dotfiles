--- Rust layer
-- @module l.rust

local plug = require("c.plug")
local file = require("c.file")

local layer = {}

-- local function on_filetype_rust()
  -- vim.wo.colorcolumn = "100"
-- end

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("ron-rs/ron.vim") -- Rusty Object Notation syntax
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local build = require("l.build")
  local lspconfig = require("lspconfig")

  -- lsp.register_server(lspconfig.rls)
  lsp.register_server(
    lspconfig.rust_analyzer,
    {
      cmd = {file.make_full("~/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin/rust-analyzer")},
      settings = {
        ["rust-analyzer"] = {
          assist = {
            importGroup = false,
          },
          checkOnSave = {
            command = "clippy",
          },
        },
      },
    }
    )

  -- Ignore cargo output
  file.add_to_wildignore("target")

  build.make_builder()
    :with_filetype("rust")
    :with_prerequisite_file("Cargo.toml")
    :with_build_command("cargo build")
    :with_test_command("cargo test")
    :add()

  -- autocmd.bind_filetype("rust", on_filetype_rust)
end

return layer
