--- The styling layer
-- @module l.style

local layer = {}

local plug = require("c.plug")
local autocmd = require("c.autocmd")

local terminal = require("l.style.terminal")
local statusline = require("l.style.statusline")
local tabline = require("l.style.tabline")

-- The startup window doesn't seem to pick up on vim.o changes >.<
local function set_default_win_opt(name, value)
  vim.o[name] = value
  autocmd.bind_vim_enter(function()
    vim.wo[name] = value
  end)
end

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("kristijanhusak/vim-hybrid-material") -- Colorscheme
  plug.add_plugin("https://gitlab.com/CraftedCart/vim-indent-guides.git") -- Indent guides
  plug.add_plugin("jaxbot/semantic-highlight.vim") -- Raiiiiiiinbow coloring!

  statusline.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- Colors
  vim.o.termguicolors = true
  autocmd.bind_colorscheme(function()
    -- Diff highlights
    vim.cmd("highlight DiffAdd ctermfg=193 ctermbg=none guifg=#66CC6C guibg=none")
    vim.cmd("highlight DiffChange ctermfg=189 ctermbg=none guifg=#B166CC guibg=none")
    vim.cmd("highlight DiffDelete ctermfg=167 ctermbg=none guifg=#CC6666 guibg=none")

    -- LSP highlights
    -- TODO: find appropriate ctermfg colors
    vim.cmd("highlight DiagnosticError ctermfg=167 ctermbg=none guifg=#CC6666 guibg=none")
    vim.cmd("highlight DiagnosticWarn ctermfg=167 ctermbg=none guifg=#CCA666 guibg=none")
    vim.cmd("highlight DiagnosticInfo ctermfg=167 ctermbg=none guifg=#66A9CC guibg=none")
    vim.cmd("highlight DiagnosticHint ctermfg=167 ctermbg=none guifg=#6B6E72 guibg=none")
    -- TODO: LspReferenceText
    -- TODO: LspReferenceRead
    -- TODO: LspReferenceWrite
  end)

  -- Shorten updatetime from the default 4000 for quicker CursorHold updates
  -- Used for stuff like the VCS gutter updates
  vim.o.updatetime = 750

  -- Allow hidden buffers
  vim.o.hidden = true

  -- Line numbers and relative line numbers
  set_default_win_opt("number", true)
  set_default_win_opt("relativenumber", true)

  -- Highlight the cursor line
  set_default_win_opt("cursorline", true)

  -- Incremental search and incremental find/replace
  vim.o.incsearch = true
  vim.o.inccommand = "split"

  -- Use case-insensitive search if the entire search query is lowercase
  vim.o.ignorecase = true
  vim.o.smartcase = true

  -- Highlight while searching
  vim.o.hlsearch = true

  -- Faster redrawing
  vim.o.lazyredraw = true

  -- Open splits on the right
  vim.o.splitright = true

  -- Show tabs and trailing whitespace
  set_default_win_opt("list", true)
  set_default_win_opt("listchars", "tab:│ ,eol: ,trail:·")
  autocmd.bind_colorscheme(function()
    vim.cmd("highlight CExtraWhitespace ctermfg=167 ctermbg=none guibg=#742B1F guifg=none")
    vim.cmd("highlight Normal guibg=NONE ctermbg=NONE") -- Make background transparent
    vim.cmd("highlight SignColumn ctermbg=NONE guibg=NONE") -- Make sign column background transparent
  end)
  autocmd.bind_win_enter(function()
    -- We probably don't care about unlisted buffers
    if vim.bo.buflisted then
      vim.cmd("match CExtraWhitespace /\\s\\+$/")
    end
  end)

  local highlight_langs = {
    "c",
    "cpp",
    "d",
    "python",
    "javascript",
    "lua",
    "rust",
  }

  autocmd.bind_buf_enter(function()
    if vim.tbl_contains(highlight_langs, vim.bo.filetype) then
      vim.cmd("SemanticHighlight")
    end
  end)
  autocmd.bind_buf_write_post(function()
    if vim.tbl_contains(highlight_langs, vim.bo.filetype) then
      vim.cmd("SemanticHighlight")
    end
  end)
  autocmd.bind_vim_enter(function()
    if vim.tbl_contains(highlight_langs, vim.bo.filetype) then
      vim.cmd("SemanticHighlight")
    end
  end)

  -- Scroll 12 lines/columns before the edges of a window
  vim.o.scrolloff = 12
  vim.o.sidescrolloff = 12

  -- Show partial commands in the bottom right
  vim.o.showcmd = true

  -- Enable mouse support
  vim.o.mouse = "a"

  -- 200ms timeout before which-key kicks in
  vim.g.timeoutlen = 200

  -- Reposition the which-key float slightly
  vim.g.which_key_floating_opts = { row = 1, col = -3, width = 3 }

  -- Always show the sign column
  set_default_win_opt("signcolumn", "yes")

  -- Configure indent guides
  vim.g.indent_guides_enable_on_vim_startup = 1
  vim.g.indent_guides_auto_colors = 0
  vim.g.indent_guides_guide_size = 1
  vim.g.indent_guides_exclude_filetypes = {
    "help",
    "nerdtree",
  }
  vim.g.indent_guides_exclude_noft = 1
  vim.g.indent_guides_default_mapping = 0
  autocmd.bind("VimEnter,Colorscheme *", function()
    vim.cmd("hi IndentGuidesEven ctermbg=0 guibg=#2E3032")
    vim.cmd("hi IndentGuidesOdd ctermbg=0 guibg=#2E3032")
  end)

  -- Transparency on the popup menus/windows
  vim.o.pumblend = 10
  vim.o.winblend = 10

  -- For Firenvim
  vim.o.guifont = "Scientifica"

  statusline.init_config()
  tabline.init_config()

  -- Make sure this is the last thing here in the style layer
  -- otherwise stuff kinda breaks on reloading...
  vim.api.nvim_command("colorscheme hybrid_reverse")
end

return layer
