--- Status line
-- @module l.style.statusline

local plug = require("c.plug")

local statusline = {}

function statusline.register_plugins()
  -- plug.add_plugin("vim-airline/vim-airline")
  -- plug.add_plugin("vim-airline/vim-airline-themes")

  plug.add_plugin("itchyny/lightline.vim")
end

function statusline.init_config()
  -- Always show the status line
  vim.o.laststatus = 2

  -- vim.g.airline_theme = "hybridline"

  -- Use vim-airline's tabline, and enable powerline symbols
  -- vim.g.airline_powerline_fonts = 1
  -- vim.g["airline#extensions#tabline#enabled"] = 1

  -- Show in the statusline the attached LSP client
  vim.api.nvim_exec(
    [[
    function! CLspGetLinePart()
      return luaeval("require('l.style.statusline')._get_line_part()")
    endfunction
    ]],
    false
    )
  -- vim.fn["airline#parts#define_function"]("c_lsp", "CLspGetLinePart")
  -- vim.fn["airline#parts#define_accent"]("c_lsp", "yellow")
  -- vim.g.airline_section_y = vim.fn["airline#section#create_right"]{"c_lsp", "ffenc"}

  vim.g.lightline = {
    enable = {
      -- TODO: Figure out some way to munge my tabline customizations in
      tabline = false,
    },
    colorscheme = "one",
    active = {
      left = {
        { "mode", "paste" },
        { "readonly", "filename" },
        { "modified" },
      },
      right = {
        { "lineinfo", "percent" },
        { "fileencoding", "fileformat" },
        { "lsp", "filetype" },
      },
    },
    component_function = {
      lsp = "CLspGetLinePart",
    }
  }
end

--- Get the LSP status line part for vim-airline
function statusline._get_line_part()
  local clients = vim.lsp.buf_get_clients()
  local client_names = {}
  for _, v in pairs(clients) do
    table.insert(client_names, v.name)
  end

  if #client_names > 0 then
    local sections = { "LSP:", table.concat(client_names, ", ") }

    local error_count = #vim.diagnostic.get(nil, {severity = vim.diagnostic.severity.ERROR})
    if error_count ~= nil and error_count > 0 then table.insert(sections, "E: " .. error_count) end

    local warn_count = #vim.diagnostic.get(nil, {severity = vim.diagnostic.severity.WARN})
    if error_count ~= nil and warn_count > 0 then table.insert(sections, "W: " .. warn_count) end

    local info_count = #vim.diagnostic.get(nil, {severity = vim.diagnostic.severity.INFO})
    if error_count ~= nil and info_count > 0 then table.insert(sections, "I: " .. info_count) end

    local hint_count = #vim.diagnostic.get(nil, {severity = vim.diagnostic.severity.HINT})
    if error_count ~= nil and hint_count > 0 then table.insert(sections, "H: " .. hint_count) end

    return table.concat(sections, " ")
  else
    return ""
  end
end

return statusline
