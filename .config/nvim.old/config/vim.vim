"set backup "Keep a backup file (restore to previous version)
set undofile "Keep an undo file (undo changes after closing)
set hidden "Allow hidden buffers
set ruler "Show the cursor position all the time
set showcmd "Display incomplete commands
set number "Display line numbers
set relativenumber "Display relative line numbers
set colorcolumn=120 "Show line at column 120
set splitright "Open vertical splits on the right
"set splitbelow "Open horizontal splits on the bottom
set foldmethod=syntax "Enable folding
set nofoldenable "Don't fold everything when opening a file
set cursorline "Show line on cursor position
set hlsearch "Switch on highlighting the last used search pattern.
set list "Show invisible chars
set listchars=tab:│\ ,eol:\ , "Set tab invisible char to an arrow and hide EOL char
set fillchars+=vert:│ "Use a full height line for the split lines
set scrolloff=12 "Start scrolling 12 lines before hitting the edge
set sidescrolloff=12 "Start scrolling 12 columns before hitting the edge
set lazyredraw "Much quicker redrawing
set wildmode=list:longest,full "Less sucky tab completion
set exrc "Use a .vimrc in the project directory
set secure "Only allow save exrc commands

syntax on "Switch syntax highlighting on

"Indentation
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab "Use spaces instead of tabs
set autoindent
set smartindent

"I like highlighting strings inside C comments.
"let c_comment_strings=1

"Enable file type detection.
"Use the default filetype settings, so that mail gets 'textwidth' set to 72,
"'cindent' is on in C files, etc.
"Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

"Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
    autocmd VimEnter * IndentGuidesEnable

    autocmd VimEnter * TagbarOpen

    "autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=black
    "autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=black

    "autocmd CompleteDone * pclose "Auto close preview window when done

    "For all text files set 'textwidth' to 120 characters.
    autocmd FileType text setlocal textwidth=120

    "Remap ,, to -> in C/C++ files
    autocmd FileType c,cpp :inoremap <buffer> ,, ->

    "Highlight trailing whitespace
    autocmd WinEnter * match ExtraWhitespace /\s\+$/

    "When editing a file, always jump to the last known cursor position.
    "Don't do it when the position is invalid or when inside an event handler
    autocmd BufReadPost *
                \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                \   execute "normal! g`\"" |
                \ endif

augroup END

"Convenient command to see the difference between the current buffer and the
"file it was loaded from, thus the changes you made.
"Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

