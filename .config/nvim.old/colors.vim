"Colorscheme
"let $NVIM_TUI_ENABLE_TRUE_COLOR=1 "Should be defined is shell config
set termguicolors

set background=dark
"colorscheme solarized
"colorscheme darcula
colorscheme hybrid_material
let g:enable_bold_font = 1 "For hybrid_material
let g:airline_theme = 'dark'

augroup vimrcEx
    "Indent guides
    autocmd Colorscheme * highlight IndentGuidesOdd  guibg=#212D32 guifg=#585858
    autocmd Colorscheme * highlight IndentGuidesEven guibg=#212D32 guifg=#585858
    "Highlight misspelt words in orange
    "autocmd ColorScheme * highlight SpellBad ctermbg=136 ctermfg=234
    "Highlight extra whitespace in red
    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=#D25252
    "Remove that ugly SignColumn highlight
    autocmd ColorScheme * highlight clear SignColumn
    "Clear background highlight - Leave it to the terminal
    autocmd ColorScheme * highlight Normal guibg=NONE ctermbg=NONE

    "Quickfix error/marker highlighting TODO: Only match in quickfix list
    autocmd ColorScheme * highlight TestMarker ctermfg=green guifg=#C4D926
    autocmd ColorScheme * highlight TestError ctermfg=red guifg=#D25252
    autocmd ColorScheme * highlight TestWarn ctermfg=yellow guifg=#FCC335

    autocmd BufNewFile,BufRead * syntax match TestMarker "\^"
    autocmd BufNewFile,BufRead * syntax match TestMarker "\~"
    autocmd BufNewFile,BufRead * syntax match TestError "error:"
    autocmd BufNewFile,BufRead * syntax match TestWarn "warning:"

    autocmd ColorScheme * highlight ALEError ctermfg=red term=underline guifg=#D25252 gui=underline
    autocmd ColorScheme * highlight ALEErrorSign ctermfg=red guifg=#D25252
    autocmd ColorScheme * highlight ALEWarn ctermfg=yellow term=underline guifg=#FCC335 gui=underline
    autocmd ColorScheme * highlight ALEWarnSign ctermfg=yellow guifg=#FCC335

    autocmd ColorScheme * highlight deniteMatchedChar ctermfg=green term=underline guifg=#C4D926 gui=underline
augroup END

