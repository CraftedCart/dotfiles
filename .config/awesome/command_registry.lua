local command_registry = {}

-- An example command
-- ==================
-- {
--   name = "hello-world",
--   action = function() print("Hello, world!") end,
-- }

command_registry.registry = {}

function command_registry.add_command(cmd)
  table.insert(command_registry.registry, cmd)
end

function command_registry.search_commands(query)
  -- Split on whitespace
  local subqueries = {}
  for match in query:gmatch("%S+") do
    table.insert(subqueries, match)
  end

  local matching_cmds = {}
  for _, cmd in pairs(command_registry.registry) do
    for _, subquery in pairs(subqueries) do
      if not cmd.name:find(subquery) then goto continue_outer end
    end

    table.insert(matching_cmds, cmd)

    ::continue_outer::
  end

  return matching_cmds
end

return command_registry
