local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local statics = require("statics")

local signals = {}

function signals.init()
  print("Initializing signals")

  -- Signal function to execute when a new client appears.
  client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position and
      not c.size_hints.program_position
      then
      -- Prevent clients from being unreachable after screen count changes.
      awful.placement.no_offscreen(c)
    end
  end)

  -- Add a titlebar if titlebars_enabled is set to true in the rules.
  client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
      awful.button({ }, 1, function()
          c:emit_signal("request::activate", "titlebar", {raise = true})
          awful.mouse.client.move(c)
      end),
      awful.button({ }, 3, function()
          c:emit_signal("request::activate", "titlebar", {raise = true})
          awful.mouse.client.resize(c)
      end)
    )

    awful.titlebar(c):setup{
      { -- Left
        awful.titlebar.widget.iconwidget(c),
        buttons = buttons,
        layout  = wibox.layout.fixed.horizontal
      },
      { -- Middle
        { -- Title
          align  = "center",
          widget = awful.titlebar.widget.titlewidget(c)
        },
        buttons = buttons,
        layout  = wibox.layout.flex.horizontal
      },
      { -- Right
        awful.titlebar.widget.floatingbutton (c),
        awful.titlebar.widget.maximizedbutton(c),
        awful.titlebar.widget.stickybutton   (c),
        awful.titlebar.widget.ontopbutton    (c),
        awful.titlebar.widget.closebutton    (c),
        layout = wibox.layout.fixed.horizontal()
      },
      layout = wibox.layout.align.horizontal
    }
  end)

  -- Enable sloppy focus, so that focus follows mouse.
  client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
  end)

  local snap_ratios = {
    { 1, 1 },
    { 4, 3 },
    { 16, 9 },
  }

  client.disconnect_signal("request::geometry", awful.mouse.resize_handler)
  client.connect_signal("request::geometry", function(c, context, hints)
    awful.mouse.resize_handler(c, context, hints)

    if context ~= "mouse.resize" then
      return
    end

    local tableutils = require("tableutils")

    local resize_size_wibox = c.resize_size_wibox
    local window_size_text = c.resize_window_size_text
    local resize_ratio_wibox = c.resize_ratio_wibox
    local window_ratio_text = c.resize_window_ratio_text
    local hide_popup_timer = c.resize_hide_popup_timer
    if resize_size_wibox == nil then
      window_size_text = wibox.widget{
        font = statics.font_title,
        align = "center",
        widget = wibox.widget.textbox,
      }
      c.resize_window_size_text = window_size_text

      window_ratio_text = wibox.widget{
        font = statics.font_subtitle,
        align = "center",
        widget = wibox.widget.textbox,
      }
      c.resize_window_ratio_text = window_ratio_text

      resize_size_wibox = wibox{
        widget = wibox.widget{
          window_size_text,

          margins = 10,
          widget = wibox.container.margin,
        },

        ontop = true,
        opacity = 0.8,
        width = 150,
        height = 40,
      }
      resize_size_wibox.input_passthrough = true
      c.resize_size_wibox = resize_size_wibox

      resize_ratio_wibox = wibox{
        widget = wibox.widget{
          window_ratio_text,

          margins = 10,
          widget = wibox.container.margin,
        },

        ontop = true,
        opacity = 0.8,
        width = 80,
        height = 40,
      }
      resize_ratio_wibox.input_passthrough = true
      c.resize_ratio_wibox = resize_ratio_wibox

      c.resize_hide_popup = function()
        resize_size_wibox.visible = false
        resize_ratio_wibox.visible = false

        c.resize_size_wibox = nil
        c.resize_window_size_text = nil
        c.resize_ratio_wibox = nil
        c.resize_window_ratio_text = nil
        c.resize_hide_popup_timer = nil
      end

      hide_popup_timer = gears.timer{
        timeout = 1,
        autostart = false,
        callback = c.resize_hide_popup,
        single_shot = true,
      }
      c.resize_hide_popup_timer = hide_popup_timer
    end

    resize_size_wibox.visible = true
    hide_popup_timer:again()

    -- Try snapping to some aspect ratios
    local did_snap = false
    for _, v in pairs(snap_ratios) do
      if math.abs((c.width / c.height) - (v[1] / v[2])) < 0.1 then
        window_ratio_text.text = v[1] .. ":" .. v[2]
        c.height = c.width / v[1] * v[2]

        resize_ratio_wibox.x = c.x + c.border_width + (c.width / 2) - (resize_ratio_wibox.width / 2)
        resize_ratio_wibox.y = c.y + c.border_width + (c.height / 2) - (resize_ratio_wibox.height / 2) + 60

        did_snap = true

        break
      end
    end

    resize_ratio_wibox.visible = did_snap

    window_size_text.text = c.width .. " x " .. c.height

    resize_size_wibox.x = c.x + c.border_width + (c.width / 2) - (resize_size_wibox.width / 2)
    resize_size_wibox.y = c.y + c.border_width + (c.height / 2) - (resize_size_wibox.height / 2)
  end)

  client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
  client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
end

return signals
