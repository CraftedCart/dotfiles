local class = require("class")

local wibox = require("wibox")
local naughty = require("naughty")
local lgi = require("lgi")

-- TODO: Remove this
local tableutils = require("tableutils")

local Gio = lgi.Gio
local GObject = lgi.GObject
local GLib = lgi.GLib
local DBusCallFlags = Gio.DBusCallFlags

-- local DEFAULT_TIMEOUT = -1
local DEFAULT_TIMEOUT = 2000

local unpack = unpack or table.unpack

local aglobalmenu = {}

-------- DBUS INTERFACE INFO --------
-- http://bazaar.launchpad.net/~indicator-applet-developers/indicator-appmenu/trunk.0.4/view/head:/src/application-menu-registrar.xml
-- https://github.com/gnustep/libs-dbuskit/blob/master/Bundles/DBusMenu/com.canonical.dbusmenu.xml
-------------------------------------

------------------------------------------------------------------------------------------------------------------------

local DBUS_REGISTRAR_SERVICE = "com.canonical.AppMenu.Registrar"
local DBUS_REGISTRAR_OBJECT_PATH = "/com/canonical/AppMenu/Registrar"

local DBUS_DBUSMENU_SERVICE = "com.canonical.dbusmenu"

------------------------------------------------------------------------------------------------------------------------

local Menu
Menu = class.strict {
  type = "standard",
  label = "",
  enabled = true,
  visible = true,
  icon_name = "",
  icon_data = class.NULL,
  shortcut = class.NULL,
  toggle_type = "",
  toggle_state = -1,
  children_display = "",

  __init = function(self)
    self.shortcut = {}
  end,
}

local WindowMenu
WindowMenu = class.strict {
  window_id = class.NULL,
  root_menu = class.NULL,

  __init = function(self, window_id)
    self.window_id = window_id
    self.root_menu = Menu()
  end,
}

------------------------------------------------------------------------------------------------------------------------

-- Maps DBus senders to a table of registered menus
-- ...something like
-- {
--   [":1:942"] = {
--     ["/MenuBar/1"] = WindowMenu,
--   },
-- }
local registered_menus = {}

local function register_menu(sender, window_id, menu_object_name)
  if registered_menus[sender] == nil then
    registered_menus[sender] = {}
  end

  registered_menus[sender][menu_object_name] = WindowMenu(window_id)
end

local function unregister_menu(sender, window_id)
  registered_menus[sender] = nil

  if registered_menus[sender] == nil then return end

  for k, v in registered_menus[sender] do
    if v.window_id == window_id then
      registered_menus[sender][k] = nil
      break
    end
  end

  if table_len(registered_menus[sender]) == 0 then
    registered_menus[sender] = nil
  end
end

------------------------------------------------------------------------------------------------------------------------

local registrar_methods = {
  RegisterWindow = function(conn, sender, object_path, interface, method, parameters, invocation)
    local window_id, menu_object_path = unpack(parameters.value)

    -- Prevent bad dbusmenu usage
    if menu_object_path == "" then
      invocation:return_value(GLib.Variant("()"))
      return
    end

    register_menu(sender, window_id, menu_object_path)

    conn:emit_signal(
      nil,
      DBUS_REGISTRAR_OBJECT_PATH,
      DBUS_REGISTRAR_SERVICE,
      "WindowRegistered",
      GLib.Variant("(uso)", {window_id, interface, menu_object_path})
      )

    -- print("TYPE IS " .. GObject.type_name(conn.class.g_type))

    invocation:return_value(GLib.Variant("()"))

    local out, err = conn:call_sync(
      sender,
      menu_object_path,
      DBUS_DBUSMENU_SERVICE,
      "GetLayout",
      GLib.Variant("(iias)", {0, -1, {}}),
      GLib.VariantType("(u(ia{sv}av))"),
      DBusCallFlags.NONE,
      DEFAULT_TIMEOUT,
      nil
      )

    if err ~= nil then
      -- TODO: Unregister
      print("dbusmenu error: " .. tostring(err))
      return
    end

    if out.type ~= "(u(ia{sv}av))" then
      -- TODO: Unregister
      print("dbusmenu error - bad return type")
      return
    end

    print("OUT " .. tableutils.to_string(out.value))
    -- for k, v in out[2][2]:pairs() do
      -- print("22OUT " .. tostring(k) .. " - " .. tableutils.to_string(v.value))
    -- end
    for k, v in out.value[2][3]:pairs() do
      print("23OUT " .. tostring(k) .. " - " .. tableutils.to_string(v.value))
    end
  end,

  UnregisterWindow = function(conn, sender, object_path, interface, method, parameters, invocation)
    local window_id = unpack(parameters.value)

    unregister_menu(sender, window_id)

    conn:emit_signal(
      nil,
      DBUS_REGISTRAR_OBJECT_PATH,
      DBUS_REGISTRAR_SERVICE,
      "WindowUnregistered",
      GLib.Variant("(u)", {window_id})
      )

    invocation:return_value(GLib.Variant("()"))
  end,
}

local function registrar_method_call(conn, sender, object_path, interface, method, parameters, invocation)
  -- TODO
  print(method)
  print(sender)
  print(object_path)
  print(tableutils.to_string(parameters.value))

  if registrar_methods[method] == nil then
    print("AppMenu Registrar method not found")
    return
  end

  local ok, err = pcall(
    registrar_methods[method],
    conn,
    sender,
    object_path,
    interface,
    method,
    parameters,
    invocation
    )
  if not ok then
    print(err)
  end
end

local function on_registrar_bus_acquire(conn, _)
  local function arg(name, signature)
    return Gio.DBusArgInfo{ name = name, signature = signature }
  end
  local method = Gio.DBusMethodInfo
  local signal = Gio.DBusSignalInfo

  local interface_info = Gio.DBusInterfaceInfo{
    name = DBUS_REGISTRAR_SERVICE,
    methods = {
      method{
        name = "RegisterWindow",
        in_args = {arg("window_id", "u"), arg("menu_object_path", "o")},
      },
      method{
        name = "UnregisterWindow",
        in_args = {arg("window_id", "u")},
      },
      -- method{
        -- name = "GetMenuForWindow",
        -- in_args = {arg("window_id", "u")},
        -- out_args = {arg("service", "s"), arg("menu_object_path", "o")},
      -- },
      -- method{
        -- name = "GetMenus",
        -- out_args = {arg("menus", "a(uso)")},
      -- },
    },
    signals = {
      signal{
        name = "WindowRegistered",
        args = {arg("window_id", "u"), arg("service", "s"), arg("path", "o")},
      },
      signal{
        name = "WindowUnregistered",
        args = {arg("window_id", "u")},
      },
    },
  }
  conn:register_object(
    DBUS_REGISTRAR_OBJECT_PATH,
    interface_info,
    GObject.Closure(registrar_method_call)
    )
end

local function on_registrar_name_acquired(conn, _) end
local function on_registrar_name_lost(_, _) end

------------------------------------------------------------------------------------------------------------------------

local function make_bar_button(text)
  return wibox.widget{
    widget = wibox.container.margin,
    left = 4,
    right = 4,

    {
      widget = wibox.widget.textbox,
      text = text,
    },
  }
end

function aglobalmenu.new_menu_bar()
  local menu_bar = wibox.widget{
    layout = wibox.layout.fixed.horizontal,

    make_bar_button("File"),
    make_bar_button("View"),
    make_bar_button("Nya~"),
  }

  return menu_bar
end

function aglobalmenu.init()
  print("Initializing aglobalmenu")

  -- Gio.bus_own_name(
    -- Gio.BusType.SESSION,
    -- DBUS_REGISTRAR_SERVICE,
    -- Gio.BusNameOwnerFlags.NONE,
    -- GObject.Closure(on_registrar_bus_acquire),
    -- GObject.Closure(on_registrar_name_acquired),
    -- GObject.Closure(on_registrar_name_lost)
    -- )
end

return aglobalmenu
