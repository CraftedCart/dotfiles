local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local easing = require("easing")

local media = require("media")
local mathutils = require("mathutils")
local anim = require("anim")

local amedia = {}

local media_title_popup = wibox.widget{
  color = beautiful.fg_normal,
  font = "sans 18",
  forced_height = 22,
  widget = wibox.widget.textbox,
}

local media_artist_popup = wibox.widget{
  color = beautiful.fg_normal,
  font = "sans 14",
  forced_height = 18,
  widget = wibox.widget.textbox,
}

local popup = awful.popup{
  widget = {
    {
      media_title_popup,
      media_artist_popup,
      layout = wibox.layout.fixed.vertical,
      spacing = 4,
    },
    margins = 10,
    widget  = wibox.container.margin
  },
  ontop = true,
  y = 100,
  visible = false,
}

function amedia.hide_popup()
  popup.visible = false
end

local hide_popup_timer = gears.timer{
  timeout = 3,
  autostart = false,
  callback = amedia.hide_popup,
  single_shot = true,
}

local function restart_popup_hide_timer()
  hide_popup_timer:again()
end

function amedia.show_popup()
  if not popup.visible then
    anim.animate{
      start_val = -64,
      end_val = 32,
      prop_table = popup,
      prop_name = "y",
      duration = 0.5,
      easing = easing.outExpo,
    }
    popup.x = 32

    popup.visible = true
  end

  media.get_title_async(function(title)
      media_title_popup.text = title
  end)

  media.get_artist_async(function(artist)
      media_artist_popup.text = artist
  end)

  restart_popup_hide_timer()
end

return amedia
