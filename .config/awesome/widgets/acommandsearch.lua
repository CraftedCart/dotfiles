local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local easing = require("easing")

local anim = require("anim")
local statics = require("statics")
local command_registry = require("command_registry")

local acommandsearch = {}

local SEARCH_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/action/2x_web/ic_search_white_48dp.png"

local search_icon_32 = {
  id = "icon",
  image = SEARCH_ICON_PATH,
  resize = true,
  forced_width = 32,
  forced_height = 32,
  widget = wibox.widget.imagebox,
}

function acommandsearch.create_command_list_item(descriptor)
  return wibox.widget{
    {
      font = statics.font_name .. " 10",
      text = descriptor.name,
      widget = wibox.widget.textbox,
    },
    spacing = 16,
    widget = wibox.layout.fixed.horizontal,
  }
end

local results_container = wibox.widget{
  spacing = 4,
  widget = wibox.layout.fixed.vertical,
}
local results_commands = {}

local selected_index = 1

function acommandsearch.update_results(query)
  results_container:reset()
  results_commands = {}

  for _, v in pairs(command_registry.search_commands(query)) do
    results_container:add(acommandsearch.create_command_list_item(v))
    table.insert(results_commands, v)
  end
end

acommandsearch.prompt = awful.widget.prompt{
  prompt = "",
  font = statics.font_name .. " 18",
  hook = {
    {{}, "Up", function(_) selected_index = selected_index - 1 end},
    {{}, "Down", function(_) selected_index = selected_index + 1 end},
  },
  done_callback = function()
    acommandsearch.hide()
  end,
  exe_callback = function(query)
    results_commands[selected_index].action()
  end,
  changed_callback = function(query)
    acommandsearch.update_results(query)
  end,
}

local popup = awful.popup{
  widget = {
    {
      {
        search_icon_32,
        acommandsearch.prompt,
        layout = wibox.layout.fixed.horizontal,
        spacing = 16,
        forced_width = 800,
      },
      results_container,
      widget = wibox.layout.fixed.vertical,
    },
    margins = 10,
    widget = wibox.container.margin,
  },
  ontop = true,
  y = 32,
  opacity = 0.95,
  visible = false,
}

function acommandsearch.hide()
  popup.visible = false
end

function acommandsearch.show()
  acommandsearch.update_results("")

  popup.visible = true
  acommandsearch.prompt:run()

  anim.animate{
    start_val = -64,
    end_val = 32,
    prop_table = popup,
    prop_name = "y",
    duration = 0.5,
    easing = easing.outExpo,
  }
  anim.animate{
    start_val = 0,
    end_val = 0.95,
    prop_table = popup,
    prop_name = "opacity",
    duration = 0.5,
    easing = easing.outExpo,
  }
  popup.x = popup.screen.geometry.width / 2 - popup.width / 2 -- Centered on the screen
end

function acommandsearch.toggle_visibility()
  if popup.visible then
    acommandsearch.hide()
  else
    acommandsearch.show()
  end
end

return acommandsearch
