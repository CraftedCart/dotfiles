local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local menubar = require("menubar")

local easing = require("easing")

local anim = require("anim")
local statics = require("statics")
local tableutils = require("tableutils")
local acommandsearch = require("widgets.acommandsearch")

local aomnimenu = {}

aomnimenu.key_name_replacements = {
  [" "] = "space",
  ["Shift"] = "S",
  ["Control"] = "C",
  ["Mod1"] = "M", -- Alt
  ["Mod4"] = "s", -- Super
}

aomnimenu.bindings = {
  ["s-space o"] = {
    name = " open file",
    action = function() end,
  },
  ["s-space s-space"] = {
    name = "search",
    action = acommandsearch.show,
  },
}

local sequence_text = wibox.widget{
  align = "center",
  font = statics.font_name .. " 18",
  widget = wibox.widget.textbox,
}

local popup = awful.popup{
  widget = {
    {
      sequence_text,
      widget = wibox.layout.fixed.vertical,
      forced_width = 800,
    },
    margins = 10,
    widget = wibox.container.margin,
  },
  ontop = true,
  y = 32,
  opacity = 0.95,
  visible = false,
}

function aomnimenu.run()
  sequence_text.text = "s-space"
  local grabber
  grabber = awful.keygrabber.run(function(mod, key, event)
      -- Ignore release keys, or modifier keys
      if event == "release" or
        key == "Super_L" or
        key == "Super_R" or
        key == "Control_L" or
        key == "Control_R" or
        key == "Shift_L" or
        key == "Shift_R" or
        key == "Alt_L" or
      key == "Alt_R" then
        return
      end

      local pressed_keys = {}
      for _, v in ipairs(mod) do
        -- Ignore shift
        if v ~= "Shift" then
          local name = aomnimenu.key_name_replacements[v] or v
          table.insert(pressed_keys, name)
        end
      end
      local name = aomnimenu.key_name_replacements[key] or key
      table.insert(pressed_keys, name)

      sequence_text.text = sequence_text.text .. " " .. table.concat(pressed_keys, "-")

      if key == "Escape" then
        awful.keygrabber.stop(grabber)
        aomnimenu.hide()
      else
        local binding = aomnimenu.bindings[sequence_text.text]
        if binding then
          awful.keygrabber.stop(grabber)
          aomnimenu.hide()

          binding.action()
        end
      end
  end)
end

function aomnimenu.hide()
  popup.visible = false
end

function aomnimenu.show()
  popup.visible = true

  anim.animate{
    start_val = -64,
    end_val = 32,
    prop_table = popup,
    prop_name = "y",
    duration = 0.5,
    easing = easing.outExpo,
  }
  anim.animate{
    start_val = 0,
    end_val = 0.95,
    prop_table = popup,
    prop_name = "opacity",
    duration = 0.5,
    easing = easing.outExpo,
  }
  popup.x = popup.screen.geometry.width / 2 - popup.width / 2 -- Centered on the screen

  aomnimenu.run()
end

function aomnimenu.toggle_visibility()
  if popup.visible then
    aomnimenu.hide()
  else
    aomnimenu.show()
  end
end

return aomnimenu
