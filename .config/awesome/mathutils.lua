local mathutils = {}

function mathutils.clamp(val, min, max)
  return math.max(math.min(val, max), min)
end

function mathutils.intround(val)
  return math.floor(val + 0.5)
end

function mathutils.lerp(a, b, t)
  return a + (b - a) * t
end

return mathutils
