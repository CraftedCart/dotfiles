local statics = require("statics")
local awful = require("awful")
local beautiful = require("beautiful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

local aluaconsole = require("widgets.aluaconsole")
local anotifarea = require("widgets.anotifarea")

local amenu = {}

-- {{{ Menu
-- Create a launcher widget and a main menu
amenu.myawesomemenu = {
  {"hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end},
  {"manual", statics.terminal .. " -e man awesome"},
  {"open Lua console", function() aluaconsole.toggle_visibility() end},
  {"open notification area", function() anotifarea.toggle_visibility() end},
  {"edit config", statics.editor_cmd .. " " .. awesome.conffile},
  {"restart", awesome.restart},
  {"quit", function() awesome.quit() end},

  theme = {
    width = 128,
  },
}

amenu.power_menu = {
  {"suspend", "systemctl suspend"},
  {"hibernate", "sh -c \"if zenity --question --title='Hibernate?' --text='Hibernate?'; then systemctl hibernate; fi\""},
  {"reboot", "sh -c \"if zenity --question --title='Reboot?' --text='Reboot?'; then systemctl reboot; fi\""},
  {"power off", "sh -c \"if zenity --question --title='Power off?' --text='Power off?'; then systemctl poweroff; fi\""},
}

amenu.mymainmenu = awful.menu{
  items = {
    {"awesome", amenu.myawesomemenu, beautiful.awesome_icon},
    {"open terminal", statics.terminal},
    {"power", amenu.power_menu},
  },
}

amenu.mylauncher = awful.widget.launcher{
  image = beautiful.awesome_icon,
  menu = amenu.mymainmenu
}

function amenu.init()
  print("Initializing menu")
  menubar.utils.terminal = statics.terminal -- Set the terminal for applications that require it
end

return amenu
